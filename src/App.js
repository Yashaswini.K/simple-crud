import TableList from "./components/TableList";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Home from "./components/Home";
import FormList from "./components/FormList";
import React, { useState } from "react";



function App() {
  const [record,setRecord] = useState({
    username: "",
    email: "",
    address: "",
    phone: "",
    gender: "",
    country: "",
    btechquly: false,
    mtechquly: false,
  })
  const [tablevalue,setTableValue]=useState([])
  const[id,setId]=useState('')

  return (
    <div className="App">
     <Router>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
                <Route exact path="/tableList" element={<TableList record={record} setRecord={setRecord} 
                tablevalue={tablevalue} setTableValue={setTableValue} id={id} setId={setId}/>}/>
                <Route exact path="/formList" element={<FormList record={record} setRecord={setRecord}
                 tablevalue={tablevalue} setTableValue={setTableValue} id={id} setId={setId}/>}/>
             </Routes>
        </Router>

    </div>
  );
}

export default App;
